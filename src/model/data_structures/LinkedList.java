package model.data_structures;

import java.util.ArrayList;

public class LinkedList<T> {
	private Node<T> first;
	
	private int size = 0;
	
	public LinkedList(T first){
		size++;
		this.first = new Node<T>( first);
	}
	
	public LinkedList(){
		first = null;
	}
	
	public int size(){
		return size;
	}
	
	public Node<T> getFirst(){
		return first;
	}
	
	public void delete(T element){
		Node<T> nodo = searchNodeWithElement(element);
		if( nodo != null){
			if(nodo == first){
				first = first.getNext();
				size--;
			}
			else{
				Node<T> previo = searchNodePreviousNode(nodo);
				previo.changeNext(nodo.getNext());
				size--;
			}
			
		}
	}
	
	public void addQueue(T element){
		if(first == null){
			first = new Node<T>(element);
			size++;
		}
		
		else{
			Node<T> nodo = searchLast();
			nodo.changeNext(new Node<T>(element));
			size++;
		}
	}
	
	public void addStack(T element){
		if(first == null){
			first = new Node<T>(element);
			size++;
		}
		else{
			Node<T> nodo = new Node<T>(element);
			nodo.changeNext(first);
			first = nodo;
			size++;
		}
	}
	
	private Node<T> searchLast(){
		Node<T >last = first;
		
		while(last != null && last.getNext() != null){
			last =  last.getNext();
		}
		
		return last;
	}
	
	public Node<T> searchNodeWithElement(T element){
		Node<T > nodo = first;
		
		while(nodo != null){
			if(nodo.getElement() == element){
				return nodo;
			}
			nodo =  nodo.getNext();
		}
		
		return null;
	}
	
	private Node<T> searchNodePreviousNode(Node<T> nodo){
		Node<T> primero = first;
		Node<T> encontrado = null;
		
		while(primero!= null && primero.getNext() != null){
			if(primero.getNext() == nodo){
				encontrado = primero;
			}
			primero = primero.getNext();
		}
		
		return encontrado;
	}
	
	public ArrayList<T> getArrayList(){
		Node<T> primero = first;
		ArrayList<T> array = new ArrayList<T>();
		if(primero != null){
			array.add(primero.getElement());
			while(primero.getNext() != null){
				primero = primero.getNext();
				array.add(primero.getElement());
			}
		}
		
		return array;
	}
	
}
