package model.data_structures;

import java.util.ArrayList;

import api.IStack;

public class Stack<T> implements IStack<T>{
	
	
	private LinkedList<T> list;
	
	public Stack(T first){
		list = new LinkedList<T>(first);	
	}
	
	public Stack(){
		list = new LinkedList<T>();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (list.size() == 0){
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public void push(T t) {
		// TODO Auto-generated method stub
		list.addStack(t);
		
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		Node<T> first1 =  list.getFirst();
		if(first1 != null){
			T element = first1.getElement();
			list.delete(element);
			return element;
		}
		else{
			return null;
		}
	}
	
	public ArrayList<T> getArrayList(){
		return list.getArrayList();
	}

}
