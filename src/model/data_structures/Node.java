package model.data_structures;

public class Node <T> {
	
	private T elemento;
	private Node<T> siguiente;
	
	public Node (T elemento){
		this.elemento = elemento;
		this.siguiente = null;
	}
	
	public Node(T elemento, Node<T> siguiente){
		this.elemento =  elemento;
		this.siguiente= siguiente;
	}
	
	public Node<T> getNext(){
		return siguiente;
	}
	
	public T getElement(){
		return elemento;
	}
	
	public Node<T> getNode(){
		return this;
	}
	
	public void changeNext(Node<T> node){
		siguiente = node;
	}
	
}