package model.data_structures;

import api.IQueue;

public class Queue<T> implements IQueue<T> {
	 
	private LinkedList<T> list;
	
	public Queue(T first){
		list = new LinkedList<T>(first);
	}
	
	public Queue(){
		list = new LinkedList<T>();
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(list.size() == 0){
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		Node<T> nodo = list.getFirst();
		if(nodo != null){
			list.delete(nodo.getElement());
			return nodo.getElement();
		}
		else{
			return null;
		}
	}

	@Override
	public void enqueue(T t) {
		// TODO Auto-generated method stub
		list.addQueue(t);
		
	}
	
}
