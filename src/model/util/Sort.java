package model.util;

public class Sort {
	
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] A ) {

		// TODO implementar el algoritmo ShellSort
		int salto, i;
		
		boolean cambios;
		for(salto=A.length/2; salto!=0; salto/=2){
			cambios=true;
			while(cambios){ 
				cambios=false;
				for(i=salto; i< A.length; i++){ 
					Comparable A1 = A[i];
					Comparable B1 = A[i-salto];
					if(less(A1,B1)){
						exchange(A,i,i-salto);
						cambios=true;
					}
				}
			}
		}
	}
	
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos, int n) {

		// TODO implementar el algoritmo MergeSort
		    if (n < 2) {
		        return;
		    }
		    int mid = n / 2;
		    Comparable[] l = new Comparable[mid];
		    Comparable[] r = new Comparable[n - mid];
		 
		    for (int i = 0; i < mid; i++) {
		        l[i] = datos[i];
		    }
		    for (int i = mid; i < n; i++) {
		        r[i - mid] = datos[i];
		    }
		    ordenarMergeSort(l, mid);
		    ordenarMergeSort(r, n - mid);
		 
		    merge(datos, l, r, mid, n - mid);	
		
	}
	
	private static void merge(Comparable[] a, Comparable[] l, Comparable[] r, int left, int right) {
			  
			    int i = 0, j = 0, k = 0;
			    while (i < left && j < right) {
			        if (less(l[i],r[j])) {
			            a[k++] = l[i++];
			        }
			        else {
			            a[k++] = r[j++];
			        }
			    }
			    while (i < left) {
			        a[k++] = l[i++];
			    }
			    while (j < right) {
			        a[k++] = r[j++];
			    }
			}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	/*public static void ordenarQuickSort( Comparable[ ] datos, int begin, int end ) {

		// TODO implementar el algoritmo QuickSort
		    if (begin < end) {
		        int partitionIndex = partition(datos, begin, end);
		 
		        ordenarQuickSort(datos, begin, partitionIndex-1);
		        ordenarQuickSort(datos, partitionIndex+1, end);
		}
	}
	
	private static int partition(Comparable [] datos, int begin, int end) {
	    Comparable pivot = datos[end];
	    int i = (begin-1);
	 
	    for (int j = begin; j < end; j++) {
	        if (less(datos[j] , pivot)) {
	            i++;
	 
	            Comparable swapTemp = datos[i];
	            datos[i] = datos[j];
	            datos[j] = swapTemp;
	        }
	    }
	 
	    Comparable swapTemp = datos[i+1];
	    datos[i+1] = datos[end];
	    datos[end] = swapTemp;
	 
	    return i+1;
	}*/
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		// TODO implementar
		int cont = v.compareTo(w);
		if(cont > 0)
			return true;
		else return false;
	}
	
	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// TODO implementar
		Comparable aux;
		aux=datos[i]; 
		datos[i]=datos[j];
		datos[j]=aux;
	}

}
