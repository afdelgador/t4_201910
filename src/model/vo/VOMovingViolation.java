package model.vo;

import java.util.Date;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	// TODO Definir los atributos de una infraccion

	private String OBJECTID;
	
	private String ROW_;

	private String LOCATION;

	private String ADDRESS_ID;

	private String STREETSEGID;

	private String XCOORD;

	private String YCOORD;

	private String TICKETTYPE;

	private String FINEAMT;

	private String TOTALPEID;

	private String PENALTY1;

	private String PENALTY2;

	private String ACCIDENTINDICATOR;

	private String TICKETSSUEDATE;

	private String VIOLATIONCODE;

	private String VIOLATIONDESC;

	/**
	 * Metodo constructor
	 */
	public VOMovingViolation(String pOBJECTID,String pROW_, String pLOCATION, String pADDRESS_ID, String pSTREETSEGID, String pXCOORD,
			String pYCOORD, String pTICKETTYPE, String pFINEAMT, String pTOTALPEID, String pPENALTY1, String pPENALTY2,String pACCIDENTINDICATOR,String pTICKETSSUEDATE ,String pVIOLATIONCODE,String pVIOLATIONDESC) {
		// TODO Implementar
		 OBJECTID= pOBJECTID;
		 ROW_=pROW_;

		 LOCATION = pLOCATION;

		 ADDRESS_ID=pADDRESS_ID;

		 STREETSEGID=pSTREETSEGID;

		 XCOORD=pXCOORD;

		 YCOORD=pYCOORD;

		 TICKETTYPE=pTICKETTYPE;

		 FINEAMT=pFINEAMT;

		 TOTALPEID=pTOTALPEID;

		 PENALTY1=pPENALTY1;

		 PENALTY2=pPENALTY2;
		 
         ACCIDENTINDICATOR=pACCIDENTINDICATOR;

		 TICKETSSUEDATE=pTICKETSSUEDATE;

		 VIOLATIONCODE=pVIOLATIONCODE;

		 VIOLATIONDESC=pVIOLATIONDESC;
		
		
	}

	/**
	 * @return id - Identificador unico de la infraccion
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return location - Direccion en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return "";
	}

	/**
	 * @return date - Fecha cuando se puso la infraccion .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return "";
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente paga el que recibio la
	 *         infraccion en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String getAccidentIndicator() {
		// TODO Auto-generated method stub
		return "";
	}

	/**
	 * @return description - Descripcion textual de la infraccion.
	 */
	public String getViolationDescription() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public int compareTo(VOMovingViolation o) {
		// TODO implementar la comparacion "natural" de la clase
		return 0;
	}

	public String toString() {
		// TODO Convertir objeto en String (representacion que se muestra en la
		// consola)
		return "-";
	}
}
