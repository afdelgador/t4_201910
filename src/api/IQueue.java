package api;

public interface IQueue<T>{
	
	
	boolean isEmpty();
	
	int size();
	
	T dequeue();

	void enqueue(T t);
	
}