package api;

import java.util.ArrayList;

public interface IStack<T> {
	
	boolean isEmpty();
	
	int size();
	
	T pop();

	void push(T t);
	
ArrayList<T> getArrayList();
	
}
