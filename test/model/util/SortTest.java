package model.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.util.Sort;

public class SortTest{

	// Muestra de datos a ordenar
	private Comparable[] datos;
	
	String[] c;
	String[] num;
	
	public void setupEscenario0(){
		c = new String[26];
		c[0] = "q";
		c[1] = "w";
		c[2] = "e";
		c[3] = "r";
		c[4] = "t";
		c[5] = "y";
		c[6]= "u";
		c[7] ="i";
		c[8] = "o";
		c[9] = "p";
		c[10] = "a" ;
		c[11] = "s";
		c[12] = "d";
		c[13] = "f";
		c[14] = "g";
		c[15] = "h";
		c[16] = "j";
		c[17] = "k";
		c[18] = "l";
		c[19] = "z";
		c[20] = "x";
		c[21] = "c";
		c[22] = "v";
		c[23] = "b";
		c[24] = "n";
		c[25] = "m";
	}
	
	public void setupEscenario1(){
		int cont = 100;
		num = new String[1000];
		
		int r = 0;
		for(int i = 0; i < 10000 ; i++){
			if(num[r] == null)
				num[r] = i+"";
			r += cont;
			if(r >= 1000){
				r = 0;
				cont--;
			}
		}
		
		/*for(int i = 0; i < 1000 ; i++){
			System.out.println(num[i]);
		}*/
	}

	@Test
	public void shellSortTest1() {
		setupEscenario1();
		Sort.ordenarShellSort(num);
		assert checkSorting(num) : "Quedo mal ordenado";

	}
	
	@Test
	public void shellSortTest0(){
		setupEscenario0();
		Sort.ordenarShellSort(c);
		
		assert checkSorting(c) : "Quedo mal ordenado";
	}
	
	@Test
	public void mergeSortTest1(){
		setupEscenario1();
		Sort.ordenarMergeSort(num, num.length);
		checkSorting(num);
	}
	
	@Test
	public void mergeSortTest0(){
		setupEscenario0();
		Sort.ordenarMergeSort(c, c.length);
		checkSorting(c);
	}
	
	/*@Test
	public void quickSortTest1(){
		setupEscenario0();
		Sort.ordenarQuickSort(c, 0, c.length);
		checkSorting(c);
	}*/

	private boolean checkSorting(String[] objeto){
		for(int i = 0; i < objeto.length && i+1 < objeto.length; i++ ){
			int val = objeto[i].compareTo(objeto[i+1]);
			if(val < 0)
				return false;
		}

		return true;
	}

}
