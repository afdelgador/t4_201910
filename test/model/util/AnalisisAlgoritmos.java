package model.util;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import model.vo.VOMovingViolation;

public class AnalisisAlgoritmos {
	
	Controller controler;
	
	
	public void inicio(){
		controler = new Controller();
		controler.run();
	}

	@Test
	public void analisisAlgoMergeSort(){
		inicio();
		Comparable<VOMovingViolation>[] muestra = controler.generarMuestra(30000);
		Sort.ordenarMergeSort(muestra, muestra.length);
		
	}

}
